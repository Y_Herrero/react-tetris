import { useState, useCallback } from 'react'
import { STAGE_WIDTH, checkCollision } from '../helpers/gameHelpers'
import { TETROMINOS } from '../hooks/useTetrominos'

export const usePlayer = (randomTetromino, sequence, setSequence) => {
  const [player, setPlayer] = useState({
    pos: {
      x: 0,
      y: 0
    },
    tetromino: [[0]],
    collided: false
  })

  const rotate = (matrix, dir) => {
    const rotatedMatrix = matrix.map((unused, i) => matrix.map(col => col[i]))
    return dir > 0
      ? rotatedMatrix.map(row => row.reverse())
      : rotatedMatrix.reverse()
  }

  const playerRotate = (stage, dir) => {
    const clonedPlayer = JSON.parse(JSON.stringify(player))
    clonedPlayer.tetromino = rotate(clonedPlayer.tetromino, dir)

    const pos = clonedPlayer.pos.x
    let offset = 1
    while (checkCollision(clonedPlayer, stage, { x: 0, y: 0 })) {
      // TODO: handle t-spin here
      clonedPlayer.pos.x += offset
      offset = -(offset + (offset > 0 ? 1 : -1))
      if (offset > clonedPlayer.tetromino[0].length) {
        rotate(clonedPlayer.tetromino, -dir)
        clonedPlayer.pos.x = pos
        return
      }
    }
    setPlayer(clonedPlayer)
  }

  const updatePlayerPos = ({ x, y, collided }) => {
    setPlayer(prev => ({
      ...prev,
      pos: {
        x: prev.pos.x + x,
        y: prev.pos.y + y
      },
      collided
    }))
  }

  const resetPlayer = useCallback(() => {
    let newTetromino = ''
    let newSequence = []
    if (sequence.length <= 0) {
      newSequence = 'IJLOSTZ'
        .split('')
        .sort((a, b) => (Math.random() > 0.5 ? -1 : 1))
      newTetromino = TETROMINOS[newSequence.shift()]
    } else {
      newSequence = [...sequence]
      newTetromino = TETROMINOS[newSequence.shift()]
    }
    setSequence(newSequence)
    setPlayer({
      pos: {
        x: STAGE_WIDTH / 2 - 2,
        y: 0
      },
      tetromino: newTetromino.shape,
      collided: false
    })
  }, [sequence, setSequence])

  return [player, updatePlayerPos, resetPlayer, playerRotate]
}
